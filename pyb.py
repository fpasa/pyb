#!/usr/bin/python

"""Welcome to pyb, the tiny, log-based, row-oriented, single-table
database written in python using only the standard library.

pyb is meant as an instructive example of how to
write a log-based storage engine for a database.

Records are stored on disk as follows: the record is prefixed with
the primary key, next there is a flag indicating the operation
("I" -> insert, "D" -> delete), then the record data is stored
as a comma separated list, with the fields in schema order.
Each record is separated by the next with a newline.

The data format is plain text for ease of understanding,
as this is an instructive example.

A special file called leading segment is an append-only log.
When a record is added, updated or deleted it is appended to this
file in the format described above. This make insertion on average
very fast because they only need to append a line to the leading
segment.

When the leading segment grows larger than MAX_SEGMENT_SIZE,
it is compacted and written to disk as a normal segment and a new
empty leading segment is created.

Compaction is a process in which redundant record are discarded,
and the records are sorted by primary key. Records are redundant
if they are overwritten or deleted later in the same log file.

Segment files are numbered with an increasing key.
Each segment file (excluding the leading segment) comes with an
index file, which contains a list of primary keys, sampled from
the segment file approximately every PAGE_SIZE bytes.

The algorithm to create this index file is as follows:

1. Start from byte zero, add the primary key stored there to the list.
2. Skip PAGE_SIZE bytes, and search for the next newline (end of record).
   Add the primary key that follows the newline to the list, together
   with its offset in the file.
3. Repeat step 2 until the file is completely processed.
4. Search for the primary key of the last record and add it to the list.
   This makes sure we can quickly skip a file when a key falls outside
   its range of keys.

When a search query is performed, the algorithm looks like this:

1. Load the leading segment into a dictionary.
2. Search if the key is in the dictionary and return the data
   if it does. If the last record is a deletion, return "not found".
3. If not found, search in the segment files, in reverse order.
   For each file, do the following:

   - Load the index file.
   - Check in which page the record is located.
   - If the page is found, load the page, scan every record to find
     the data and return it if found. Return not found if there is
     a deletion record (e.g. marked with a "D" flag).
   - If not found, or the segment does not contain the record,
     continue to the next segment.

4. If not found, return "not found".

Some notes:

* There might be multiple insertions (e.g. update),
  but the algorithm above will return the latest one.
* There might be an insertion and a deletion in the same segment,
  in which case the compaction will keep the deletion only.
* There might be an insertion in a log file and a deletion in
  different files, but just the deletion will be found.

One could implement a global compaction process that runs once
in a while, removing duplicate insertions and discarding records
marked as deleted. This in not implemented, but would ensure
the size of the data is kept small if there are a lot of updates
or deletions of existing records.
"""
import sys
import json
import time
from argparse import ArgumentParser
from contextlib import contextmanager
from pathlib import Path


FOLDER_HELP = "Folder where the table is stored"
TYPE_MAP = {"str": str, "int": int, "float": float}
MAX_SEGMENT_SIZE = 2**23  # 8 MB
PAGE_SIZE = 2**12  # 4 KB


def main():
    args = parse_args()
    args.folder.mkdir(exist_ok=True, parents=True)
    args.func(args)


def parse_args():
    parser = ArgumentParser("pyb")
    subparsers = parser.add_subparsers(help="Command to run")

    create_table = subparsers.add_parser(
        "create-table", help="Create table with the given schema"
    )
    create_table.add_argument("folder", help=FOLDER_HELP)
    create_table.add_argument(
        "field",
        nargs="+",
        help=(
            "Field to create, in the format 'name:type'. "
            "Valid types are 'str', 'int', 'float'"
        ),
    )
    create_table.set_defaults(func=cmd_create_table)

    schema = subparsers.add_parser("schema", help="Print the table schema")
    schema.add_argument("folder", help=FOLDER_HELP)
    schema.set_defaults(func=cmd_schema)

    add = subparsers.add_parser("add", help="Add records to the table")
    add.add_argument("folder", help=FOLDER_HELP)
    add.add_argument(
        "values",
        nargs="?",
        help=(
            "Field values to add, in the same order as defined in the schema. "
            "Different field are comma-separated, commas can be escaped with "
            "'\\'. Alternatively, you can pass records to standard input, "
            "one row per record, same field format."
        ),
    )
    add.set_defaults(func=cmd_add)

    add = subparsers.add_parser("del", help="Delete records from the table")
    add.add_argument("folder", help=FOLDER_HELP)
    add.add_argument(
        "primary_key",
        nargs="?",
        help=(
            "Primary key to remove. Alternatively, you can pass primary keys "
            "to standard input, one key per row."
        ),
    )
    add.set_defaults(func=cmd_del)

    add = subparsers.add_parser("get", help="Get records from the table")
    add.add_argument("folder", help=FOLDER_HELP)
    add.add_argument(
        "primary_key",
        nargs="?",
        help=(
            "Primary key to remove. Alternatively, you can pass primary keys "
            "to standard input, one key per row."
        ),
    )
    add.add_argument(
        "--raw",
        default=False,
        action="store_true",
        help="Don't pretty print records",
    )
    add.set_defaults(func=cmd_get)

    add = subparsers.add_parser("index", help="Create index on a column")
    add.add_argument("folder", help=FOLDER_HELP)
    add.add_argument("column", help="Name of the column to index")
    add.set_defaults(func=cmd_index)

    add = subparsers.add_parser("filter", help="Filter records based on conditions")
    add.add_argument("folder", help=FOLDER_HELP)
    add.add_argument(
        "conditions",
        nargs="+",
        help=(
            "One or multiple conditions in the form 'field=value' "
            "to filter records on."
        ),
    )
    add.add_argument(
        "--raw",
        default=False,
        action="store_true",
        help="Don't pretty print records",
    )
    add.set_defaults(func=cmd_filter)

    args = parser.parse_args(sys.argv[1:] or ["--help"])
    args.folder = Path(args.folder)
    return args


def cmd_create_table(args):
    meta_path = args.folder / "meta.json"
    if meta_path.exists():
        err(f"Table in '{args.folder}' already exist")

    schema = {}
    for field in args.field:
        if ":" not in field:
            err(f"Invalid field '{field}' (should be in 'name:type' format)")

        name, ty = field.split(":")
        if ty != "str" and ty != "int" and ty != "float":
            err(f"Invalid type '{ty}' for field '{name}'")
        schema[name] = ty

    write_meta(args.folder, schema, 0, [])
    print(f"Created table '{args.folder.name}'")


def cmd_schema(args):
    schema, _, _ = load_meta(args.folder)
    print(f"{'FIELD':<24} TYPE")
    print("\n".join([f"{field:<24} {ty}" for field, ty in schema.items()]))


def cmd_add(args):
    schema, largest_key, indices = load_meta(args.folder)
    records = get_input_items(args.values, lambda v: parse_values(v, schema))
    i = 0
    while i < len(records) - 1:
        with open_leading_segment(args.folder) as file:
            for i, values in enumerate(records[i:], start=i):
                largest_key = largest_key + 1
                set_record(file, largest_key, values)
                print(largest_key)

                # Every now and then, check if we have to compact
                # otherwise inserting 1 GB will cause 1 GB segments
                if i % 20000:
                    break

        check_if_compaction_is_needed(args.folder, schema)

    write_meta(args.folder, schema, largest_key, indices)


def cmd_del(args):
    schema, _, _ = load_meta(args.folder)
    keys = get_input_items(args.primary_key, parse_key)

    # Check that keys exist
    get_records(args.folder, schema, keys)

    with open_leading_segment(args.folder) as file:
        for key in keys:
            delete_record(file, key)
            print(key)

    check_if_compaction_is_needed(args.folder, schema)


def cmd_get(args):
    keys = get_input_items(args.primary_key, parse_key)
    schema, _, _ = load_meta(args.folder)
    records = get_records(args.folder, schema, keys)
    print_records(records, schema, args.raw)


def cmd_index(args):
    schema, _, _ = load_meta(args.folder)
    create_index(
        folder,
    )


def cmd_filter(args):
    schema, _, _ = load_meta(args.folder)
    conditions = {}
    for c in args.conditions:
        field, value = c.split("=", 1)
        conditions[field] = value
    records = filter_records(args.folder, schema, conditions)
    print_records(records, schema, args.raw)


def write_meta(folder, schema, largest_key, indices):
    meta_path = folder / "meta.json"
    meta_path.write_text(
        json.dumps(
            {
                "schema": schema,
                "__LARGEST_KEY__": largest_key,
                "indices": indices,
            }
        )
    )


def load_meta(folder):
    meta_path = folder / "meta.json"
    meta = json.loads(meta_path.read_text())
    return meta.pop("schema"), meta.pop("__LARGEST_KEY__"), meta.pop("indices")


def get_input_items(arg, func):
    if arg:
        return [func(arg)]

    return [func(l[:-1]) for l in sys.stdin]


def parse_values(values, schema):
    values = [
        # Hacky way to honor comma escapes
        v.replace("__COMMA__", ",")
        for v in values.replace("\\,", "__COMMA__").split(",")
    ]
    for i, (value, (field, ty)) in enumerate(zip(values, schema.items())):
        try:
            value = value.strip()
            values[i] = TYPE_MAP[ty](value) if value else 0
        except TypeError:
            err(
                f"Field '{field}' has invalid value '{value}' "
                f"(should be of type '{ty}')"
            )
    return values


def parse_key(key):
    try:
        return int(key)
    except TypeError:
        err(f"Primary key '{key}' is invalid")


def set_record(file, key, values):
    write_record(file, key, "I", values)


def delete_record(file, key):
    write_record(file, key, "D", [])


def write_record(file, key, flag, values):
    values_text = ",".join(
        # escape commas for strings
        v.replace(",", "\\,") if isinstance(v, str) else str(v)
        for v in values
    )
    record = f"{key},{flag},{values_text}\n"
    file.write(record)
    return len(record)


def get_records(folder, schema, keys):
    leading_segment = load_leading_segment(folder, schema)
    indices = load_indices(folder)
    return [get_single(folder, leading_segment, indices, key, schema) for key in keys]


def get_single(folder, leading_segment, indices, key, schema):
    if key in leading_segment:
        flag = leading_segment[key]["flag"]
        if flag == "I":
            return leading_segment[key]["values"]
        else:
            # Key was deleted
            err(f"Key '{key}' was not found")

    for segment_num, index in indices:
        # Exclude file quickly
        first_key = index[0][0]
        last_key = index[-1][0]
        if key < first_key or key > last_key:
            continue

        last_offset = 0
        for index_key, offset in index[1:]:
            if key < index_key:
                # This means we found the page
                flag, data = search_page(
                    folder, segment_num, last_offset, offset, key, schema
                )
                if flag == "I":
                    return data
                else:
                    # Key was deleted
                    err(f"Key '{key}' was not found")

            last_offset = offset

    err(f"Key '{key}' was not found")


def filter_records(folder, schema, conditions):
    records = []
    added_keys = {}

    for segment_path in sorted(folder.glob("segment_*"), reverse=True):
        for key, flag, values in iter_records(segment_path, schema):
            if (
                flag == "I"
                and key not in added_keys
                and check_conditions(values, schema, conditions)
            ):
                records.append(values)

    return records


def load_leading_segment(folder, schema):
    """Load leading segment as dictionary"""
    leading_segment_path = get_leading_path(folder)
    if not leading_segment_path.exists():
        return {}

    leading_segment = {}
    for key, flag, values in iter_records(leading_segment_path, schema):
        leading_segment[key] = dict(flag=flag, values=values)
    return leading_segment


def load_indices(folder):
    indices = []
    index_paths = folder.glob("index_*")
    for index_path in sorted(index_paths, reverse=True):
        index = []
        with open(index_path, "r") as file:
            for line in file:
                key, offset = line[:-1].split(",")
                index.append((int(key), int(offset)))

        segment_num = int(index_path.name.lstrip("index_"))
        indices.append((segment_num, index))

    return indices


def get_leading_path(folder):
    return folder / "leading"


@contextmanager
def open_leading_segment(folder, mode="a"):
    with open(get_leading_path(folder), mode) as file:
        yield file


def iter_records(path, schema):
    with open(path, "r") as file:
        for line in file:
            key, flag, data = line[:-1].split(",", 2)
            values = parse_values(data, schema)
            yield int(key), flag, values


def check_conditions(values, schema, conditions):
    for value, field in zip(values, schema.keys()):
        if field in conditions:
            if value != conditions[field]:
                return False

    return True


def search_page(folder, segment_num, page_start, page_end, key, schema):
    length = page_end - page_start
    with open(folder / f"segment_{segment_num}") as file:
        file.seek(page_start)
        page = file.read(length)

    for line in page.splitlines(keepends=False):
        if line.startswith(str(key)):
            values = parse_values(line, schema)
            return values[1], values[2:]

    return None


def check_if_compaction_is_needed(folder, schema):
    leading_segment_path = get_leading_path(folder)
    if leading_segment_path.stat().st_size > MAX_SEGMENT_SIZE:
        compact(folder, schema)


def compact(folder, schema):
    # Loading to dictionary already compacts in the sense
    # that it keeps only the last value of a key and discard
    # deleted keys
    leading_segment = load_leading_segment(folder, schema)

    segment_files = list(folder.glob("segment_*"))
    new_segment_num = len(segment_files) + 1

    new_segment_path = folder / f"segment_{new_segment_num}"
    offset = 0
    from_last_offset = PAGE_SIZE  # To force index to contain first key
    index = []
    with open(new_segment_path, "w") as file:
        for key, record in sorted(leading_segment.items(), key=lambda i: i[0]):
            if from_last_offset >= PAGE_SIZE:
                index.append((key, offset))
                from_last_offset = 0

            diff = write_record(file, key, record["flag"], record["values"])
            offset += diff
            from_last_offset += diff

        # Add last key to index
        index.append((key, offset - diff))

    index_path = folder / f"index_{new_segment_num}"
    with open(index_path, "w") as file:
        for a, b in index:
            file.write(f"{a},{b}\n")

    # Remove leading segment causing next calls to create a new empty one
    get_leading_path(folder).unlink()


def print_records(records, schema, raw):
    if raw:
        print(
            "\n".join(
                [
                    # Escape commas
                    ",".join(str(field).replace(",", "\\,") for field in record)
                    for record in records
                ]
            )
        )
    else:
        for field in schema:
            print(f"{field:<24}", end="")
        print()

        for record in records:
            for field in record:
                print(f"{field:<24}", end="")
            print()

        print("Number of records:", len(records))


def err(msg):
    print(msg, file=sys.stderr)
    sys.exit(1)


if __name__ == "__main__":
    start = time.perf_counter()
    main()
    print(time.perf_counter() - start)
