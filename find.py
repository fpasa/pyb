import time


def main():
    count = 0
    with open("data/car_crashes/data.csv") as file:
        for data in file:
            fields = data.split(",")
            if fields[14] == "Brakes Defective":
                print(data)
                count += 1
    print(count)


if __name__ == "__main__":
    start = time.perf_counter()
    main()
    print(time.perf_counter() - start)
